using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialActiveActive : MonoBehaviour
{

    [SerializeField] GameObject tutorial;
    [SerializeField] GameObject onj;

    [SerializeField] Animator anima;
    [SerializeField] string animaName;
    bool wasActive;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (tutorial.activeSelf == true && wasActive == false)
        {
            if(onj != null)
            {
                wasActive = true;

            onj.SetActive(true);
            }
            if(anima != null)
            {
                wasActive = true;
                anima.Play(animaName);
            }
        }
    }
}
