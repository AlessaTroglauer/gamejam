using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationCheck : MonoBehaviour
{
    [SerializeField] GameObject lastText;
    [SerializeField] Animator animate;
    [SerializeField] string animationName;

    [SerializeField] bool wasActive;
    

    public GameObject gameManager;
    public GameObject QTEOne;
    //[SerializeField] GameObject ceph;

    private void Update()
    {
        if(lastText.activeSelf == true)
        {
            wasActive = true;
        }

        if(wasActive == true && lastText.activeSelf == false)
        {
            wasActive = false;
            animate.Play(animationName);
        }

        if(gameManager != null && gameManager.GetComponent<GameManager>().hasEnded == true)
        {
            QTEOne.SetActive(false);
            gameManager.SetActive(false);

        }

    }

}
