using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimEvents : MonoBehaviour
{
    [SerializeField] GameObject dia;

    [SerializeField] Animator anima;
    [SerializeField] string animaName;

    [SerializeField] GameObject dis;
    [SerializeField] GameObject en;

    [SerializeField] AudioSource source;
    [SerializeField] AudioClip clip;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void StartDia()
    {
        dia.SetActive(true);
    }

    void StartAnim()
    {
        anima.Play(animaName);
    }

    void Disable()
    {
        dis.SetActive(false);
    }

    void Enable()
    {
        en.SetActive(true);
    }

    void PlayAudio()
    {
        source.clip = clip;
        source.Play();
    }
}
