using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDeactive : MonoBehaviour
{

    [SerializeField] GameObject tutorial;
    [SerializeField] GameObject onj;
    bool wasActive;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (tutorial.activeSelf == true)
        {
            wasActive = true;
        }
        if (tutorial.activeSelf == false && wasActive == true)
        {
            onj.SetActive(false);
            wasActive = false;
        }
    }
}
