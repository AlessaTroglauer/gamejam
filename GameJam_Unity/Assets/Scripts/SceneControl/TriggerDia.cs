using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDia : MonoBehaviour
{
    [SerializeField] GameObject dia;
    [SerializeField] GameObject qteOne;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TriggerDialog()
    {
        dia.SetActive(true);
    }

    public void TriggerEventOne()
    {
        qteOne.SetActive(true);
    }
}
