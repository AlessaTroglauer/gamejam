using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] GameObject tutorial;
    [SerializeField] Animator animator;
    [SerializeField] string animName;

    bool wasActive;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(tutorial.activeSelf == true)
        {
            wasActive = true;
        }
        if (tutorial.activeSelf == false && wasActive == true)
        {
            animator.Play(animName);
            wasActive = false;
        }
    }
}
