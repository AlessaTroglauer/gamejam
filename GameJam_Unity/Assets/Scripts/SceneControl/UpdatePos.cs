using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdatePos : MonoBehaviour
{
    [SerializeField] Vector3 newPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SetPosi();
    }

    void UpdatePosi()
    {
        newPos = gameObject.transform.position;
    }

    void SetPosi()
    {
        gameObject.transform.position = newPos;
    }
}
