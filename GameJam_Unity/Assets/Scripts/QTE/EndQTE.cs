using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

public class EndQTE : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Activator")
        {
            GameManager.instance.NoteHit();
        }
    }
}



