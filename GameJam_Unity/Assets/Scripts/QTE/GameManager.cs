using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;


    public GameObject textPanel;
    public GameObject failureText;
    public GameObject successText;

    public GameObject noteHolder;
    public GameObject CephOld;
    public GameObject Ceph;

    public Animator cam;

    public bool hasEnded;

    private void Start()
    {
        hasEnded = false;
        instance = this;
        textPanel.SetActive(false); 
    }

    public void NoteHit()
    {
        if (Ceph != null && CephOld != null)
        {
            CephOld.SetActive(false);
            Ceph.SetActive(true);

        }
        hasEnded = true;

        Debug.Log("Hit Note");

        textPanel.SetActive(true);
        successText.SetActive(true);
        if (cam != null)
        {
            cam.Play("CephTalkingCounter");
        }

    }

    public void NoteMissed()
    {
        if(Ceph != null && CephOld != null)
        {
            CephOld.SetActive(false);
            Ceph.SetActive(true);

        }

        hasEnded = true;

        Debug.Log("Missed Note");

        textPanel.SetActive(true);
        failureText.SetActive(true);

        noteHolder.SetActive(false); 
        if(cam != null)
        {
            cam.Play("CephTalkingCounter");
        }
    }
}
