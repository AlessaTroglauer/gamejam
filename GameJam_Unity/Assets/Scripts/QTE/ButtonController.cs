using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public GameObject defaultText;
    public GameObject pressedText;

    public KeyCode keyToPress; 

    void Start()
    {
    }

    void Update()
    {
        if (Input.GetKeyDown(keyToPress))
        {
            defaultText.SetActive(false);
            pressedText.SetActive(true);
        }

        if (Input.GetKeyUp(keyToPress))
        {
            defaultText.SetActive(true);
            pressedText.SetActive(false);
        }
    }
}
