using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    [SerializeField] private GameObject dialog;

    void TriggerDialog()
    {
        dialog.SetActive(true);
    }
}
