using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog : MonoBehaviour
{
    public GameObject[] convo;

    [SerializeField] int convoNumber = 1;
    int prevNumber = 0;
    [SerializeField] int maxnumber;

    private bool canPlay;

    //[SerializeField] Transform mainCam;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.LookAt(mainCam);
        if(convo[0].activeSelf == true)
        {
            canPlay = true;
        }

        if(canPlay == true && Input.GetButtonDown("Jump"))
        {
            if(convoNumber < maxnumber)
            {
                convo[convoNumber].SetActive(true);
            }

            convoNumber += 1;

            if(convoNumber >= 2)
            {
                convo[prevNumber].SetActive(false);
                prevNumber += 1;
            }
        }

        if(convoNumber > maxnumber)
        {
            gameObject.SetActive(false);
        }
    }
}
