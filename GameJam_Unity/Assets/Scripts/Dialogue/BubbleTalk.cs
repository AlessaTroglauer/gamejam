using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleTalk : MonoBehaviour
{
    [SerializeField] GameObject bubble;
    [SerializeField] Animator animator;
    [SerializeField] string animName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(bubble.activeSelf == true)
        {
            animator.Play(animName);
        }
    }
}
