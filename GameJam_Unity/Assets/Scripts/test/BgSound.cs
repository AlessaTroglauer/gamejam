﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgSound : MonoBehaviour
{
    //static data type as the instance is constant
    private static BgSound instance = null;
    //Property of our Instance
    public static BgSound Instance
    {
        get { return instance; }
    }

    //Awake is called when the script instance is being loaded
    private void Awake()
    {
        //if our instance is not null and attached to a gameobject
        //This method will assure that there arent two GO with this script attached in a scene
        if(instance != null && instance != this)
        {
            //Destroy the gameobject
            Destroy(this.gameObject);
            return;
        }
        else
        {
            //Sets the intance to the gameobject its attached to
            instance = this;
        }
    }
    //Update is called once per frame
    private void Update()
    {
        //Doesnt destroy the gameobject with this script when loading a new scene
        DontDestroyOnLoad(this.gameObject);
    }
}
