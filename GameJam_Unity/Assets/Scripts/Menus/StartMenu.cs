using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    [SerializeField] private GameObject credits;
    [SerializeField] Animator cameraAnim;
    [SerializeField] Animator cephAnim;

    public void StartFirstScene()
    {
        Debug.Log("Starting");
        cameraAnim.Play("Scene01");
        cephAnim.Play("Scene01Counter");
        gameObject.SetActive(false);
    }

    public void CreditsDisplay()
    {
        credits.SetActive(true);
    }

    public void CreditsHide()
    {
        credits.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
