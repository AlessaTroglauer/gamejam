using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialActvieDeactive : MonoBehaviour
{
    [SerializeField] GameObject tutorial;
    [SerializeField] GameObject onj;
    bool wasActive;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (tutorial.activeSelf == true && wasActive == false)
        {
                wasActive = true;

                onj.SetActive(false);
        }
    }
}
